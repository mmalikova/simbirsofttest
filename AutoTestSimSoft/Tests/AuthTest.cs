﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Support;
using AutoTestSimSoft.Pages;
using System.Threading;


namespace AutoTestSimSoft
{
    [TestClass]
    public class AuthTest
    {
        IWebDriver driver;

        // checks existence of elements on the page
        [TestMethod]
        public void CheckAuthFields()
        {
            AuthPage auth = new AuthPage(driver);
            Assert.IsTrue(auth.GetLoginField() != null && auth.GetPasswordField() != null && auth.GetSigninButton() != null && auth.GetRegisterLink() !=null);
        }

        //check valid link "Registration"
        [TestMethod]
        public void CheckRegistrationLink()
        {
            AuthPage auth = new AuthPage(driver);
            Assert.AreEqual("http://fcs.simbirsoft1.com/#registration", auth.ClickRegistrationLink());
        }

        // try access to "closed" page without authorization
        [TestMethod]
        public void CheckAuthRedirect()
        {
            driver.Navigate().GoToUrl("http://fcs.simbirsoft1.com/#transactions");
            driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(15));
            Thread.Sleep(5000);
            Assert.AreEqual("http://fcs.simbirsoft1.com/#login", driver.Url);
        }


        // check simple auth with valid fields
        [TestMethod]
        public void AuthWithValidInputs()
        {           
            // Try to register user with login/password - to be sure that the user exists
            RegistrationPage reg = new RegistrationPage(driver);

            UserRegisterData uData = new UserRegisterData("1234qwerty@mail.com", "1234qwerty", "123456qwe", "123456qwe", "0");
            if (reg.DoRegister(uData))
            {
                TransactionsPage tpage = new TransactionsPage(driver);
                tpage.DoLogOut();
            }

            AuthPage auth = new AuthPage(driver);
            UserLoginData userData = new UserLoginData("1234qwerty", "123456qwe");
            Assert.IsTrue(auth.DoLogOn(userData));

            // and log out 
            TransactionsPage trpage = new TransactionsPage(driver);
            Assert.IsTrue(trpage.DoLogOut());
        }                    

        // login with invalid fields
        [TestMethod]
        public void AuthWithInValidFields() 
        {
            AuthPage auth = new AuthPage(driver);

            UserLoginData userData = new UserLoginData("", "");            
            Assert.IsFalse(auth.DoLogOn(userData));

            userData = new UserLoginData("", "123456qwe");
            Assert.IsFalse(auth.DoLogOn(userData));

            userData = new UserLoginData("1234qwerty", "");
            Assert.IsFalse(auth.DoLogOn(userData));

            // password in login field / login to password field
            userData = new UserLoginData("123456qwe", "1234qwerty");
            Assert.IsFalse(auth.DoLogOn(userData));

            // login field: <script>alert(123)<script>
            userData = new UserLoginData("<script>alert(123)<script>", "1234qwerty");
            Assert.IsFalse(auth.DoLogOn(userData));

            // login field: <form action=”http://test.ru”><input type=”submit”></form>
            userData = new UserLoginData("<form action=”http://test.ru”><input type=”submit”></form>", "1234qwerty");
            Assert.IsFalse(auth.DoLogOn(userData));

            // password field with script
            userData = new UserLoginData("123456qwe", "<script>alert(123)<script>");
            Assert.IsFalse(auth.DoLogOn(userData));

            // password field with html
            userData = new UserLoginData("123456qwe", "<form action=”http://test.ru”><input type=”submit”></form>");
            Assert.IsFalse(auth.DoLogOn(userData));

            // space in fields
            userData = new UserLoginData("   123456qwe  ", "  1234qwerty   ");
            Assert.IsFalse(auth.DoLogOn(userData));

            // correct login with capslock
            userData = new UserLoginData("123456QWE", "1234qwerty");
            Assert.IsFalse(auth.DoLogOn(userData));

            // corect password with caps
            userData = new UserLoginData("123456qwe", "1234QWERTY");
            Assert.IsFalse(auth.DoLogOn(userData));            
        }       
                
        
        [TestInitialize]
        public void Setup()
        {
            // start browser and open URL
            driver = new FirefoxDriver();
        }

        [TestCleanup]
        public void CleanupTest()
        {
            // close Browser
            driver.Quit();
        }
    }
}
