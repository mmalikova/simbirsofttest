﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Support;
using AutoTestSimSoft.Pages;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AutoTestSimSoft
{
    [TestClass]
    public class RegTest
    {
        IWebDriver driver;

        string newUser1Login;        
        string newUser1Email;
        string newUser1Pass;
        string newUser2Login;
        string newUser2Email;
        string newUser2Pass;        
        string newUser3Login;
        string newUser3Email;
        string newUser3Pass;

        public void InitializeVariables() 
        {
            // this variables shoud be unique each test run - generate them
            string unixTimeNow = ((Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds).ToString();
            newUser1Login = "user" + unixTimeNow;
            newUser1Email = newUser1Login + "@mail.com";
            newUser1Pass = newUser1Login + "pass";

            newUser2Login = "user" + unixTimeNow + "ABC";
            newUser2Email = newUser2Login + "@mail.com";
            newUser2Pass = newUser2Login + "pass";

            newUser3Login = "user" + unixTimeNow + "DEF";
            newUser3Email = newUser3Login + "@mail.com";
            newUser3Pass = newUser3Login + "pass";

        }

        // checks existence of elements on the page
        [TestMethod]
        public void CheckRegFields()
        {
            RegistrationPage reg = new RegistrationPage(driver);
            Assert.IsTrue(reg.GetEmailField() != null &&
                reg.GetLoginField() != null &&
                reg.GetPasswordField() != null &&
                reg.GetRepeatPasswordField() != null &&
                reg.GetCapitalField() != null &&
                reg.GetRegistarButton() != null);
        }

        // try to register new user with startup capital
        [TestMethod]
        public void RegisterUserWithCapital() 
        {
            RegistrationPage reg = new RegistrationPage(driver);
            UserRegisterData userData = new UserRegisterData(newUser1Email, newUser1Login, newUser1Pass, newUser1Pass, "100.25");
            
            Assert.IsTrue(reg.DoRegister(userData));

            // and log out
            TransactionsPage tpage = new TransactionsPage(driver);
            Assert.IsTrue(tpage.DoLogOut());
        }


        // try to register new user without startup capital
        [TestMethod]
        public void RegisterUserWithoutCapital()
        {
            RegistrationPage reg = new RegistrationPage(driver);
            UserRegisterData userData = new UserRegisterData(newUser2Email, newUser2Login, newUser2Pass, newUser2Pass, "0");
            Assert.IsTrue(reg.DoRegister(userData));

            // and log out
            TransactionsPage tpage = new TransactionsPage(driver);
            Assert.IsTrue(tpage.DoLogOut());
        }


        // Try to register with some empty fields
        [TestMethod]
        public void RegisterWithEmptyFields() 
        {
            RegistrationPage reg = new RegistrationPage(driver);
            UserRegisterData userData = new UserRegisterData("", "", "", "", "");
            Assert.IsFalse(reg.DoRegister(userData));

            userData = new UserRegisterData(newUser3Email, "", "", "", "");
            Assert.IsFalse(reg.DoRegister(userData));

            userData = new UserRegisterData("", newUser3Login, "", "", "");
            Assert.IsFalse(reg.DoRegister(userData));

            userData = new UserRegisterData("", "", newUser3Pass, "", "");
            Assert.IsFalse(reg.DoRegister(userData));

            userData = new UserRegisterData("", "", "", newUser3Pass, "");
            Assert.IsFalse(reg.DoRegister(userData));

            userData = new UserRegisterData(newUser3Email, newUser3Login, "", "", "");
            Assert.IsFalse(reg.DoRegister(userData));

            userData = new UserRegisterData(newUser3Email, "", newUser3Pass, "", "");
            Assert.IsFalse(reg.DoRegister(userData));

            userData = new UserRegisterData(newUser3Email, "", "", newUser3Pass, "");
            Assert.IsFalse(reg.DoRegister(userData));

            userData = new UserRegisterData("", newUser3Login, newUser3Pass, newUser3Pass, "0.00");
            Assert.IsFalse(reg.DoRegister(userData));

            userData = new UserRegisterData(newUser3Email, "", newUser3Pass, newUser3Pass, "0.00");
            Assert.IsFalse(reg.DoRegister(userData));

            userData = new UserRegisterData(newUser3Email, newUser3Login, "", newUser3Pass, "0.00");
            Assert.IsFalse(reg.DoRegister(userData));

            userData = new UserRegisterData(newUser3Email, newUser3Login, newUser3Pass, "", "0.00");
            Assert.IsFalse(reg.DoRegister(userData));
        }

        // try to register with some invalid fields
        [TestMethod]
        public void RegisterWithInvalidFields()
        {
            RegistrationPage reg = new RegistrationPage(driver);
            
            // mail            
            UserRegisterData userData = new UserRegisterData("usermail.com", newUser3Login, newUser3Pass, newUser3Pass, "0.00");
            Assert.IsFalse(reg.DoRegister(userData));

            userData = new UserRegisterData("u.ser@mail", newUser3Login, newUser3Pass, newUser3Pass, "0.00");
            Assert.IsFalse(reg.DoRegister(userData));

            userData = new UserRegisterData("<script>alert(123)<script>", newUser3Login, newUser3Pass, newUser3Pass, "0.00");
            Assert.IsFalse(reg.DoRegister(userData));

            userData = new UserRegisterData("<form action=”http://test.ru”><input type=”submit”></form>", newUser3Login, newUser3Pass, newUser3Pass, "0.00");
            Assert.IsFalse(reg.DoRegister(userData));            

            // login
            userData = new UserRegisterData(newUser3Email, "qwertyqwertyqwertyqwertyqwertyqwertyqwertyqwertyqwertyqwertyqwertyqwertyqwertyqwertyqwertyqwertyqwertyqwertyqwertyqwertyqwertyqwertyqwertyqwertyqwertyqwertyqwertyqwertyqwertyqwertyqwertyqwertyqwertyqwertyqwertyqwertyqwertyqwertyqwertyqwertyqwertyqwertyqwerty", newUser3Pass, newUser3Pass, "0.00");
            Assert.IsFalse(reg.DoRegister(userData));

            userData = new UserRegisterData(newUser3Email, "qw", newUser3Pass, newUser3Pass, "0.00");
            Assert.IsFalse(reg.DoRegister(userData));

            userData = new UserRegisterData(newUser3Email, "user123U;<>?/*-+", newUser3Pass, newUser3Pass, "0.00");
            Assert.IsFalse(reg.DoRegister(userData));

            userData = new UserRegisterData(newUser3Email, "<script>alert(123)<script>", newUser3Pass, newUser3Pass, "0.00");
            Assert.IsFalse(reg.DoRegister(userData));

            userData = new UserRegisterData(newUser3Email, "<form action=”http://test.ru”><input type=”submit”></form>", newUser3Pass, newUser3Pass, "0.00");
            Assert.IsFalse(reg.DoRegister(userData));
            
            // pass
            userData = new UserRegisterData(newUser3Email, newUser3Login, "1234", "1234", "0.00");
            Assert.IsFalse(reg.DoRegister(userData));

            userData = new UserRegisterData(newUser3Email, newUser3Login, "12345678910111213141516171819202122", "12345678910111213141516171819202122", "0.00");
            Assert.IsFalse(reg.DoRegister(userData));

                     

            // repPass
            userData = new UserRegisterData(newUser3Email, newUser3Login, "user123456", "UsEr123456", "0.00");
            Assert.IsFalse(reg.DoRegister(userData));

            userData = new UserRegisterData(newUser3Email, newUser3Login, "user123456", "user1234567", "0.00");
            Assert.IsFalse(reg.DoRegister(userData));

            //capital
            userData = new UserRegisterData(newUser3Email, newUser3Login, newUser3Pass, newUser3Pass, "-150.20");
            Assert.IsFalse(reg.DoRegister(userData));

            userData = new UserRegisterData(newUser3Email, newUser3Login, newUser3Pass, newUser3Pass, "150,20");
            Assert.IsFalse(reg.DoRegister(userData));

            userData = new UserRegisterData(newUser3Email, newUser3Login, newUser3Pass, newUser3Pass, "150.456");
            Assert.IsFalse(reg.DoRegister(userData));

            userData = new UserRegisterData(newUser3Email, newUser3Login, newUser3Pass, newUser3Pass, "not a number");
            Assert.IsFalse(reg.DoRegister(userData));

            userData = new UserRegisterData(newUser3Email, newUser3Login, newUser3Pass, newUser3Pass, "<script>alert(123)<script>");
            Assert.IsFalse(reg.DoRegister(userData));

            userData = new UserRegisterData(newUser3Email, newUser3Login, newUser3Pass, newUser3Pass, "<form action=”http://test.ru”><input type=”submit”></form>");
            Assert.IsFalse(reg.DoRegister(userData));            

        }

        // try to register with non-unique values
        [TestMethod]
        public void RegisterWithNotUnicValues()
        {
            // Try to register user with login/password - to be sure that the user exists
            RegistrationPage reg = new RegistrationPage(driver);

            UserRegisterData uData = new UserRegisterData("testUser@test.com", "testUser", "testUserPass", "testUserPass", "0");
            if (reg.DoRegister(uData))
            {
                TransactionsPage tpage = new TransactionsPage(driver);
                tpage.DoLogOut();
            }

            reg = new RegistrationPage(driver);

            // mail            
            UserRegisterData userData = new UserRegisterData("testUser@test.com", newUser3Login, newUser3Pass, newUser3Pass, "0.00");
            Assert.IsFalse(reg.DoRegister(userData));

            // login
            userData = new UserRegisterData(newUser3Email, "testUser", newUser3Pass, newUser3Pass, "0.00");
            Assert.IsFalse(reg.DoRegister(userData));

            // pass
            userData = new UserRegisterData(newUser3Email, newUser3Login, "testUserPass", "testUserPass", "0.00");
            Assert.IsFalse(reg.DoRegister(userData));
        }

        [TestInitialize]
        public void Setup()
        {
            // start browser 
            InitializeVariables();
            driver = new FirefoxDriver();
        }

        [TestCleanup]
        public void CleanupTest()
        {
            // close Browser
            driver.Quit();
        }
    }
}
