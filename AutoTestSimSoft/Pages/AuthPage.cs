﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support;
using System.Threading;

namespace AutoTestSimSoft.Pages
{
    class AuthPage
    {
        // This class describes the authorization page

        IWebDriver driver;

        private IWebElement loginField;
        private IWebElement passwordField;
        private IWebElement signinButton;
        private IWebElement registerLink;

        public AuthPage(IWebDriver driver)
        {
            this.driver = driver;
            driver.Navigate().GoToUrl("http://fcs.simbirsoft1.com/");
            driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(3));
            var allTextFields = driver.FindElements(By.ClassName("my_textbox"));
            loginField = allTextFields[0];
            passwordField = allTextFields[1];
            signinButton = driver.FindElement(By.ClassName("loginButton"));
            registerLink = driver.FindElement(By.LinkText("Registration"));

        }

        public IWebElement GetLoginField() { return loginField; }
        public IWebElement GetPasswordField() { return passwordField; }
        public IWebElement GetSigninButton() { return signinButton; }
        public IWebElement GetRegisterLink() { return registerLink; }

        public bool DoLogOn(UserLoginData userData)
        {
            loginField.Clear();
            passwordField.Clear();
            loginField.SendKeys(userData.GetLogin());
            passwordField.SendKeys(userData.GetPassword());
            driver.FindElement(By.ClassName("loginButton")).Click();
            driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(3));
            Thread.Sleep(5000);
            try
            {
                var error = driver.FindElement(By.ClassName("error_label"));
                return false;
            }
            catch (Exception ex)
            {
                return true;
            }            
            return true;
        }        

        public string ClickRegistrationLink()
        {
            registerLink.Click();
            driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(3));
            string url = driver.Url;
            return url;
        }
    }
}
