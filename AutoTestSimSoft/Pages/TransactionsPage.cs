﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support;

namespace AutoTestSimSoft.Pages
{
    class TransactionsPage
    {
        IWebDriver driver;

        private IWebElement logoutButton;

        public TransactionsPage(IWebDriver driver)
        {
            this.driver = driver;
            logoutButton = driver.FindElement(By.ClassName("gwt-Button"));
        }

        public bool DoLogOut()
        {
            try
            {
                logoutButton.Click();
                var loginBtn = driver.FindElement(By.ClassName("loginButton"));
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }
    }
}
