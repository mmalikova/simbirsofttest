﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support;
using System.Threading;

namespace AutoTestSimSoft.Pages
{
    class RegistrationPage
    {
        // This class describes the registration page

        IWebDriver driver;

        private IWebElement emailField;
        private IWebElement loginField;
        private IWebElement passwordField;
        private IWebElement repeatPasswordField;
        private IWebElement capitalField;
        private IWebElement registerButton;

        public RegistrationPage(IWebDriver driver)
        {
            this.driver = driver;
            driver.Navigate().GoToUrl("http://fcs.simbirsoft1.com/#registration");
            driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(3));
            var allTextFields = driver.FindElements(By.ClassName("my_textbox"));
            emailField = allTextFields[0];
            loginField = allTextFields[1];
            passwordField = allTextFields[2];
            repeatPasswordField = allTextFields[3];
            capitalField = allTextFields[4];            
            registerButton = driver.FindElement(By.ClassName("loginButton"));
            
        }

        public IWebElement GetEmailField() { return emailField; }
        public IWebElement GetLoginField() { return loginField; }
        public IWebElement GetPasswordField() { return passwordField; }
        public IWebElement GetRepeatPasswordField() { return repeatPasswordField; }
        public IWebElement GetCapitalField() { return capitalField; }
        public IWebElement GetRegistarButton() { return registerButton; }

        public bool DoRegister(UserRegisterData userData)
        {
            emailField.Clear();
            emailField.SendKeys(userData.GetEmail());
            loginField.Clear();
            loginField.SendKeys(userData.GetLogin());
            passwordField.Clear();
            passwordField.SendKeys(userData.GetPassword());
            repeatPasswordField.Clear();
            repeatPasswordField.SendKeys(userData.GetRepeatPassword());
            capitalField.Clear();
            capitalField.SendKeys(userData.GetCapital());            
            registerButton.Click();
            driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(3));
            Thread.Sleep(5000);
           
            try
            {
                var error = driver.FindElement(By.ClassName("error_label"));
                return false;                
            }
            catch (Exception ex)
            {
                var wallet = driver.FindElement(By.XPath(@"//*[@id=""application""]/table/tbody/tr[2]/td[3]/table/tbody/tr[2]/td/div"));
                string sum = wallet.Text.Remove(0, 8);
                if (sum == userData.GetCapital())
                    return true;
                return false;
            }
            return true;
        }

    }
}
