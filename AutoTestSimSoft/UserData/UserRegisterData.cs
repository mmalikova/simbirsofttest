﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutoTestSimSoft
{
    class UserRegisterData
    {
        private string email;
        private string login;
        private string password;
        private string repeatPassword;
        private string capital;

        public UserRegisterData(string email, string login, string password, string repeatPassword, string capital)
        {
            this.email = email;
            this.password = password;
            this.login = login;
            this.repeatPassword = repeatPassword;
            this.capital = capital;
        }

        public string GetEmail() { return this.email; }
        public string GetLogin() { return this.login; }
        public string GetPassword() { return this.password; }
        public string GetRepeatPassword() { return this.repeatPassword; }
        public string GetCapital() { return this.capital; }
        public void SetEmail(string email) { this.email = email; }
        public void SetLogin(string login) { this.login = login; }
        public void SetPassword(string password) { this.password = password; }
        public void SetRepeatPassword(string repeatPassword) { this.repeatPassword = repeatPassword; }
        public void SetCapital(string capital) { this.capital = capital; }
    }
}
