﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutoTestSimSoft
{
    class UserLoginData
    {
        private string login;
        private string password;

        public UserLoginData(string login, string password) {
            this.password = password;
            this.login = login;
        }

        public string GetLogin() { return this.login; }
        public string GetPassword() { return this.password; }
        public void SetLogin(string login) { this.login = login; }
        public void SetPassword(string password) { this.password = password; }
    }
}
